module Potepan::ProductDecorator
  Spree::Product.class_eval do
    scope :add_price_image, -> { includes(master: [:default_price, :images]) }

    def related_products
      Spree::Product.in_taxons(taxons).add_price_image.where.not(id: id).distinct
    end
  end

  Spree::Product.prepend self
end
