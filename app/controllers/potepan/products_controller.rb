class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.first
    @related_products = @product.related_products.limit(Const::MAX_RELATED_PRODUCTS_COUNT)
  end
end
