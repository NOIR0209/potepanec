require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_1) { create(:taxon, taxonomy: taxonomy) }
  let(:taxon_2) { create(:taxon, taxonomy: taxonomy) }
  let!(:product_1) { create(:product, taxons: [taxon_1]) }
  let!(:product_2) { create(:product, taxons: [taxon_2]) }

  describe '#show' do
    before do
      get potepan_category_path(taxon_1.id)
    end

    it 'responds successfully' do
      expect(response).to be_successful
    end

    it 'gets @taxon' do
      expect(controller.instance_variable_get('@taxon')).to eq taxon_1
    end

    it 'gets @products' do
      expect(controller.instance_variable_get('@products')).to contain_exactly(product_1)
    end

    it 'shows taxonomy name' do
      expect(response.body).to include taxonomy.name
    end

    it 'shows taxon name' do
      expect(response.body).to include taxon_1.name
      expect(response.body).to include taxon_2.name
    end

    it 'shows product name relate to taxon_1' do
      expect(response.body).to include product_1.name
    end

    it "doesn't show product name not relate to taxon_1" do
      expect(response.body).not_to include product_2.name
    end
  end
end
