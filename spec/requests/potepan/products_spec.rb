require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  describe '#show' do
    before do
      get potepan_product_path(product.id)
    end

    it 'responds successfully' do
      expect(response).to be_successful
    end

    it 'gets @product' do
      expect(controller.instance_variable_get('@product')).to eq product
    end

    it 'shows product name' do
      expect(response.body).to include product.name
    end

    it 'shows related_product name' do
      expect(response.body).to include related_product.name
    end
  end
end
