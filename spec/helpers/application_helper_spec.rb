require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title' do
    it "returns given arguments and ' | BIGBAG Store'" do
      expect(full_title('Example')).to eq "Example | BIGBAG Store"
    end

    it "returns only 'BIGBAG Store' if not receives any arguments" do
      expect(full_title('')).to eq 'BIGBAG Store'
    end

    it "returns only 'BIGBAG Store' if receives nil" do
      expect(full_title(nil)).to eq 'BIGBAG Store'
    end
  end
end
