require 'rails_helper'

RSpec.feature "mutual layout", type: :feature do
  feature 'mutual layout parts' do
    background do
      visit potepan_path
    end

    scenario 'click "BIGBAG" logo and visit top page' do
      click_on 'BIGBAG'
      expect(current_path).to eq potepan_path
    end
  end
end
