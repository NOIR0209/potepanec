require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxons) { taxonomy.root.children.create }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxons.id) }
  let(:product) { create(:product, price: 999.99, description: 'Foo.', taxons: [taxon]) }
  let!(:related_product_1) { create(:product, taxons: [taxon]) }
  let!(:related_product_2) { create(:product, taxons: [taxon]) }
  let!(:related_product_3) { create(:product, taxons: [taxon]) }
  let!(:related_product_4) { create(:product, taxons: [taxon]) }
  let!(:related_product_5) { create(:product, taxons: [taxon]) }
  let!(:non_related_product) { create(:product) }

  feature '#show' do
    background do
      visit potepan_product_path(product.id)
    end

    scenario 'visits top page from the light section in detail page' do
      within('.breadcrumb') do
        click_on 'Home'
        expect(current_path).to eq potepan_path
      end
    end

    scenario 'shows accurate products details' do
      expect(page).to have_title "#{product.name} | BIGBAG Store"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    scenario 'moves to categories page if back button is clicked' do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    scenario 'shows related products' do
      within('.productsContent') do
        expect(page).to have_content related_product_1.name
        expect(page).to have_content related_product_1.display_price
      end
    end

    scenario "doesn't show non related products"  do
      within('.productsContent') do
        expect(page).not_to have_content non_related_product.name
      end
    end

    it 'shows related products up to MAX_RELATED_PRODUCTS_COUNT' do
      within('.productsContent') do
        expect(page).to have_selector '.productBox', count: Const::MAX_RELATED_PRODUCTS_COUNT
      end
    end

    scenario 'moves to the related products show page if that name is clicked' do
      within('.productsContent') do
        click_on related_product_1.name
        expect(current_path).to eq potepan_product_path(related_product_1.id)
      end
    end
  end
end
