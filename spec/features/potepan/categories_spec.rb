require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxons) { taxonomy.root.children.create(name: "Categories") }
  let(:taxon_1) { create(:taxon, name: "Perfume", taxonomy: taxonomy, parent_id: taxons.id) }
  let(:taxon_2) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxons.id) }
  let!(:product_1) { create(:product, name: "Black Opium Eau de Perfume", price: "75.35", taxons: [taxon_1]) }
  let!(:product_2) { create(:product, name: "Leather Suitcase", price: "650.00", taxons: [taxon_2]) }

  feature '#show' do
    background do
      visit potepan_category_path(taxon_1.id)
    end

    scenario 'shows accurate taxon details' do
      expect(page).to have_title "#{taxon_1.name} | BIGBAG Store"
      expect(page).to have_content taxon_1.name
      expect(page).to have_content taxon_2.name
      expect(page).to have_content product_1.name
      expect(page).to have_content product_1.name
    end

    scenario 'doesnt show any products information which doesnt relate to this pages taxon' do
      expect(page).not_to have_content product_2.name
      expect(page).not_to have_content product_2.price
    end

    scenario 'visits top page from the light section in category page' do
      within('.breadcrumb') do
        click_on 'Home'
        expect(current_path).to eq potepan_path
      end
    end

    scenario 'moves to products page if product name is clicked' do
      click_on product_1.name
      expect(current_path).to eq potepan_product_path(product_1.id)
    end

    scenario 'clicks sidebar and taxon name, and then moves to that taxons page', js: true do
      within('.side-nav') do
        click_on taxonomy.name
        click_on taxon_1.name
        expect(current_path).to eq potepan_category_path(taxon_1.id)
      end
    end
  end
end
