require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_1) { create(:taxon, taxonomy: taxonomy) }
  let(:taxon_2) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon_1]) }
  let!(:related_product) { create(:product, taxons: [taxon_1]) }
  let!(:non_related_product) { create(:product, taxons: [taxon_2]) }

  it 'gets related products with related_products method' do
    expect(product.related_products).to include related_product
  end

  it "doesn't include that product in related products" do
    expect(product.related_products).not_to include product
  end

  it "doesn't include non related product" do
    expect(product.related_products).not_to include non_related_product
  end
end
